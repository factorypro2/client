import axios from "axios";

axios.defaults.baseURL = "https://factorypro-server.onrender.com/";

export default axios;