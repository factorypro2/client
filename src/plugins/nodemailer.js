const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  host: "smtp.ethereal.email",
  port: 587,
  secure: false, // Use `true` for port 465, `false` for all other ports
  auth: {
    user: "jackgrealish28112002@gmail.com",
    pass: "Nomon28112002",
  },
});

// async..await is not allowed in global scope, must use a wrapper
export default function sendEmail(receiver, userData){
    async function main() {
        // send mail with defined transport object
        const messageBody = `<h1>Factory Pro</h1><p>Dear ${userData.firstName},</p>
        <br>
        <p>We hope this message finds you well.</p>
        <br>
        <p>As requested, we're reaching out to provide you with your login credentials for <strong>FactoryPro</strong>:</p>
        <ul>
            <li><strong>Username:</strong> ${userData.username}</li>
            <li><strong>Password:</strong> ${userData.password}</li>
        </ul><br>
        <p>Please, reset your password in profile settings.</p>
        <br>
        <p>Welcome to <strong>www.factorypro.uz</strong>. We look forward to serving you.</p>
        <br>
        <p>Best regards,<br>
        Alex<br>
        FactoryPro Team</p>
        <br>`


        const info = await transporter.sendMail({
          from: '"FactoryPro Admin 👻" <jackgrealish28112002@gmail.com>', // sender address
          to: receiver, // list of receivers
          subject: "FactoryPro Account Created ✔", // Subject line
          html: messageBody // html body
        });
      
        console.log("Message sent: %s", info.messageId);
        // Message sent: <d786aa62-4e0a-070a-47ed-0b0666549519@ethereal.email>
      }
}


main().catch(console.error);