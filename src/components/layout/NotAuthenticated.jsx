import StopSign from '../../assets/stop_sign.svg'
import { useTranslation } from "react-i18next";


export default function NotAuthenticated(){
    const { t } = useTranslation();

    return (
        <div className='notauth_wrapper'>
            <img className='notauth_image' src={StopSign} alt="icon" />
            <h2>{t("auth_notauth", "Not Authenticated")}.<br/> {t("auth_please", "Please")}, <a href="/login">{t("auth_login", "Log In")}</a></h2>
        </div>
    )
}

