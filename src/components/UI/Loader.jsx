import loaderimg from '../../assets/loader.webp'
import "./Loader.css"

export default function Loader(){
    return (
        <div className='loader_wrapper'>
            <img src={loaderimg} alt="loader" className='loader_img' />
        </div>
    )
}